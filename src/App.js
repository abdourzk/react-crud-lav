
import './App.css';
import { useState, useEffect } from 'react';

function App() {
  const [B, setB] = useState([])

  useEffect(async () => {
    const A = fetch("http://127.0.0.1:8000/api/t")
    A.then(e => e.json()).then(e => Array(setB(e)))

    // const [item] = data.categories;
    // setB(item);

  }, [])
  return (
    <div className="App">
      <header className="App-header">
        <table  >
          <tbody>
          <tr>
            <th>ID.</th>
            
            <th>Libellé</th>
          </tr>
          <tr>
            <td>
          {B.map(u => <h5 key={Math.random()}>{u.idc}</h5>)}
          </td>
          <td>
          {B.map(u => <h5 key={Math.random()}>{u.libelle}</h5>)}
          </td>
          </tr>
          </tbody>
        </table>

      </header>
    </div>
  );
}

export default App;
